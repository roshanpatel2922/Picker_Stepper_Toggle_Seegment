//
//  ContentView.swift
//  Picker_Stepper_Toggle_Seegment
//
//  Created by Roshan Patel on 27/02/23.
//

import SwiftUI

struct ContentView: View {
    @State var Gender = 1
    @State var Switch = true
    @State var age = 18
    @State var A = 0
    
    var body: some View {
        VStack {
            Toggle("Switch on for Customize",isOn: $Switch ).onChange(of: Switch) { i in }
            if Switch {
                Stepper("Age", value: $age, in: (18...50),step: 1)
                Text("\(age)")
            }
            if Switch {
                Picker("Gender", selection: $Gender) {
                    Text("Male").tag(0)
                    Text("FeMale").tag(1)
                    Text("Others").tag(2)
                }
            }
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
