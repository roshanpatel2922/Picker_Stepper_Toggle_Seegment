//
//  Picker_Stepper_Toggle_SeegmentApp.swift
//  Picker_Stepper_Toggle_Seegment
//
//  Created by Roshan Patel on 27/02/23.
//

import SwiftUI

@main
struct Picker_Stepper_Toggle_SeegmentApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
